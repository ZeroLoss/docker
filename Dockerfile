FROM nvidia/cuda:8.0-cudnn6-devel-ubuntu16.04

WORKDIR /
ENV OPENCV_VERSION="3.4.2"

# Get apt dependancies
RUN apt-get update && \ 
    apt-get install -y build-essential \
    python \
    python-pip \
    cmake \
    git \
    wget \
    unzip \
    yasm \
    libgtk2.0-dev \
    pkg-config \
    libswscale-dev \
    libtbb2 \
    libtbb-dev \
    libjpeg-dev \
    libpng-dev

# OpenCV install
RUN pip install numpy && \
    wget https://github.com/opencv/opencv/archive/${OPENCV_VERSION}.zip && \
    unzip ${OPENCV_VERSION}.zip && \
    mkdir /opencv-${OPENCV_VERSION}/cmake_binary && \
    cd /opencv-${OPENCV_VERSION}/cmake_binary && \
    cmake -DBUILD_TIFF=ON \
        -DBUILD_opencv_java=OFF \
        -DWITH_CUDA=ON \
        -DBUILD_opencv_apps=ON \
        -DWITH_OPENGL=ON \
        -DWITH_OPENCL=ON \
        -DWITH_IPP=ON \
        -DWITH_TBB=ON \
        -DWITH_EIGEN=ON \
        -DWITH_V4L=ON \
        -DBUILD_TESTS=OFF \
        -DBUILD_PERF_TESTS=OFF \
        -DCMAKE_BUILD_TYPE=RELEASE \
        -DCMAKE_INSTALL_PREFIX=$(python -c "import sys; print(sys.prefix)") \
        -DPYTHON_EXECUTABLE=$(which python) \
        -DPYTHON_INCLUDE_DIR=$(python -c "from distutils.sysconfig import get_python_inc; print(get_python_inc())") \
        -DPYTHON_PACKAGES_PATH=$(python -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())") \
        ..  && \
    make -j $(($(nproc) + 1)) && \
    make install && \
    rm /${OPENCV_VERSION}.zip && \
    rm -r /opencv-${OPENCV_VERSION}

# Darknet install
RUN git clone https://github.com/pjreddie/darknet.git && \
# might want to reset to good commit
    cd darknet && \
    sed -i -e 's+GPU=0+GPU=1+g' /darknet/Makefile && \
    sed -i -e 's+CUDNN=0+CUDNN=1+g' /darknet/Makefile && \
    sed -i -e 's+OPENCV=0+OPENCV=1+g' /darknet/Makefile && \
    ln -s /usr/local/cuda-8.0 /usr/local/cuda && \
    make && \
    sed -i -e 's+"libdarknet.so"+"/darknet/libdarknet.so"+g' /darknet/python/darknet.py
# TODO: edit makefile to include CUDA and OPENCV later

COPY . /
