# Setup
This Docker container needs to be run on a Linux distribution to use the kernel. It has only been tested in Ubuntu 16.04, but should probably work in more recent versions and most other distros.

## GPU
Using the GPU makes training and detection time a lot faster, but takes a long time to build the Docker and may be annoying to setup.

If you're not using an NVIDIA GPU or don't want to suffer through a long build time, you'll need to use the `Dockerfile-nogpu` instead. Replace the existing `Dockerfile` with this.

If using GPU, make sure you have the appropriate NVIDIA drivers. I found the correct drivers to use with `sudo ubuntu-drivers devices` and installed the third-party free recommended driver. [This guide](https://websiteforstudents.com/install-proprietary-nvidia-gpu-drivers-on-ubuntu-16-04-17-10-18-04/) may be helpful, although it suggested the wrong driver for my machine, everything else is useful.

You'll also need to [install NVIDIA Docker](https://chunml.github.io/ChunML.github.io/project/Installing-NVIDIA-Docker-On-Ubuntu-16.04/) if you want to use GPU. Otherwise normal Docker will work.

# Usage
Create a docker container with:
`docker build -t kiwifruit .`

Run the image to create a container in the background:
`docker run -d -t --name kiwifruit_container --runtime=nvidia -v <path-to-folder>:/data:rw --net=host --env="DISPLAY" --volume="$HOME/.Xauthority:/root/.Xauthority:rw" kiwifruit`
In this instance <path-to-folder> is the absolute path to the location of a file you would like to mount inside the docker container.
We recommend copying any data you want to use into/from the /data directory.
You can later pause and resume this container when you need to.

After this, you can run the following commands to interact with the container:

 - To get a shell: `docker exec -it kiwifruit_container /bin/bash`
 - To run detection on a group of images (file or directory) `docker exec -t kiwifruit_container python scripts/detect.py <PATH> [OUTPUT_PATH]`. If output path is specified, a folder will be created for the annotated images to be saved. We recommend moving images to somewhere in /data and having an output path also in /data. The directory will need to exist already, so you might want to use `docker exec -it kiwifruit_container mkdir /data/...`
 - To annotate more image positives if you ever want to do more training, move images to /data/images and use `docker exec kiwifruit_container opencv_annotation --images=/data/images --annotations=/data/annotations.txt --maxWindowHeight=1000 --resizeFactor=5` and follow the instructions in stdout to generate more annotations. Then, use the command `docker exec kiwifruit_container python scripts/convert.py` to set up all the training data.
 - To train the network further, use `docker exec kiwifruit_container /darknet/darknet detector train /darknet/cfg/obj.data /darknet/cfg/flower-yolov3-tiny.cfg /darknet/backup/flower-yolov3-tiny.backup`. You'll need to delete or move the existing backup file if you want to start training completely fresh (say if you're trying to identify a different class).
 
# Notes
The test/training images and their associated labels are included in this container, although they're not strictly necessary. You can test the container using these images however.
 
 - The `opencv_anotation` tool can be tested with with `docker exec kiwifruit_container opencv_annotation --images=/data/test --annotations=/data/annotations.txt --maxWindowHeight=1000 --resizeFactor=5`
 - The `detect.py` script can be tested with `docker exec -t kiwifruit_container python scripts/detect.py /images/test /data`. The annotated images will be saved in `/data`.
