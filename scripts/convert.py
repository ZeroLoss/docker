# Intelligently generate:
# /darknet/train.txt
# /data/labels
# Rename backup file

import os
import cv2
import shutil

if __name__ == '__main__':
    if not os.path.exists('/data/labels'):
        os.makedirs('/data/labels')

    shutil.copyfile('/darknet/train.txt', '/darknet/train-backup.txt')

    with open('/data/annotations.txt', 'r') as ann, open('/darknet/train.txt', 'w') as train:
        for line in ann.readlines():
            # Read the parts of the line
            # filename count [count instances of x y w h]
            parts = line.split()
            fn = parts[0]
            num = int(parts[1])
            img = cv2.imread(fn)
            name, _ = os.path.splitext(os.path.basename(fn))
            train.write(fn + '\n')

            with open('/data/labels/'+name+'.txt', 'w') as label:
                for n in range(num):
                    x, y, w, h = list(map(float, parts[2 + n * 4:6 + n * 4]))

                    # Expand into a square
                    if w < h:
                        diff = h - w
                        w = h
                        x -= diff / 2
                        # Ensure inside left/right bounds
                        if x < 0:
                            x = 0
                        elif x + w >= img.shape[1]:
                            x = img.shape[1] - w - 1
                    elif h < w:
                        diff = w - h
                        h = w
                        y -= diff / 2
                        # Ensure inside top/bottom bounds
                        if y < 0:
                            y = 0
                        elif y + h >= img.shape[0]:
                            y = img.shape[0] - h - 1

                    xmid = x + w / 2.0
                    ymid = y + h / 2.0
                    height = img.shape[0]
                    width = img.shape[1]

                    label.write('0 ' + str(xmid / width) + ' ' + str(ymid / height) + ' ' + str(w / width) + ' ' + str(h / height) + '\n')
