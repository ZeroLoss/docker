import imp
import sys
import os
import cv2

darknet = imp.load_source('darknet', '/darknet/python/darknet.py')

allowed_extensions = ['.jpg', '.png', '.jpeg']

if __name__ == '__main__':
    path = sys.argv[1]
    save_path = False if len(sys.argv) <= 2 else sys.argv[2]

    net = darknet.load_net('/darknet/cfg/flower-yolov3-tiny.cfg', '/darknet/flower-yolov3-tiny.backup', 0)
    meta = darknet.load_meta('/darknet/cfg/obj.data')

    def do_detect(fn):
        _, ext = os.path.splitext(fn)
        if ext.lower() in allowed_extensions:
            r = darknet.detect(net, meta, fn)
            if save_path and r:
                img = cv2.imread(fn)

                for label in r:
                    x, y, w, h = list(map(int, label[2]))
                    x -= w/2
                    y -= h/2
                    cv2.rectangle(img, (x, y), (x+w, y+h), (255, 0, 255), 10)

                cv2.imwrite(os.path.join(save_path, os.path.basename(fn)), img)
            print fn + ':' + str(len(r))
            return len(r)
        return 0

    count = 0

    if os.path.isfile(path):
        count += do_detect(path)
    elif os.path.isdir(path):
        for fn in os.listdir(path):
            count += do_detect(os.path.join(path, fn))

    print 'total:' + str(count)
